﻿using ClienteMvcApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClienteMvcApi.Services
{
    public interface IRegionRepositoryGUI
    {
        Task<IEnumerable<RegionDto>> GetRegionsAsync();
    }
}
