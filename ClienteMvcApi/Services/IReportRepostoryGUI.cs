﻿using ClienteMvcApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClienteMvcApi.Services
{
    public interface IReportRepostoryGUI
    {
        Task<ReportDto> GetReportByParamAsync(string iso = null, string region_province=null, string region_name = null, string city_name = null, DateTime? date = null, string q = null);
    }
}
