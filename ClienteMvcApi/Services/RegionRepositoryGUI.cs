﻿using ClienteMvcApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ClienteMvcApi.Services
{
    public class RegionRepositoryGUI : IRegionRepositoryGUI
    {
        public async Task<IEnumerable<RegionDto>> GetRegionsAsync()
        {
            IEnumerable<RegionDto> regions = new List<RegionDto>();
           
            using (var client = new HttpClient())
            {
                //client.BaseAddress = new Uri("https://covid-19-statistics.p.rapidapi.com/");


                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri("https://covid-19-statistics.p.rapidapi.com/regions"),
                    Headers = 
                    {
                        { "x-rapidapi-key", "099d451921msh53f5d89d1242704p149afcjsn6a19aaa0baf4" },
                        { "x-rapidapi-host", "covid-19-statistics.p.rapidapi.com" },
                    },
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var body = await response.Content.ReadAsAsync<DataDto>(); //await response.Content.ReadAsStringAsync();

                    regions = body.data;

                    //Console.WriteLine(body);
                }
            }

            return regions;
        }
    }

}
