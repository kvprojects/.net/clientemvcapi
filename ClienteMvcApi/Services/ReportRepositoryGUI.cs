﻿using ClienteMvcApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ClienteMvcApi.Services
{
    public class ReportRepositoryGUI : IReportRepostoryGUI
    {
        public async Task<ReportDto> GetReportByParamAsync(string iso, string region_province=null, string region_name = null, string city_name = null, DateTime? date = null, string q = null)
        {
            ReportDto report = new ReportDto();

            using (var client = new HttpClient())
            {
                //client.BaseAddress = new Uri("https://covid-19-statistics.p.rapidapi.com/");


                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri("https://covid-19-statistics.p.rapidapi.com/reports"),
                    //RequestUri = new Uri("https://covid-19-statistics.p.rapidapi.com/reports/{region_province}/{iso}/{region_name}/{city_name}/{date}/{q}"),
                    Headers =
                    {
                        { "x-rapidapi-key", "099d451921msh53f5d89d1242704p149afcjsn6a19aaa0baf4" },
                        { "x-rapidapi-host", "covid-19-statistics.p.rapidapi.com" },
                    },
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var body = await response.Content.ReadAsAsync<ReportDataDto>(); //await response.Content.ReadAsStringAsync();

                    report = body.data.FirstOrDefault();

                    //Console.WriteLine(body);
                }
            }

            return report;
        }
    }
}
