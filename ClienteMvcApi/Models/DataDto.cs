﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClienteMvcApi.Models
{
    public class DataDto
    {
        public List<RegionDto> data;
        public ProvinceDto dataProvince;
        public TotalReportDto dataTotalReport;

    }
}
