﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClienteMvcApi.Models
{
    public class ReportDataDto
    {
        public List<ReportDto> data;
    }
}
