﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClienteMvcApi.Models
{
    public class TotalReportDto
    {
        public DateTime date { get; set; }
        public DateTime last_update { get; set; }
        public int confirmed { get; set; }
        public int confirmed_diff { get; set; }
        public int deaths { get; set; }
        public int deaths_diff { get; set; }
        public int recovered { get; set; }        
        public int recovered_diff { get; set; }        
        public int active { get; set; }
        public int active_diff { get; set; }
        public decimal fatality_rate { get; set; }
    }
}
