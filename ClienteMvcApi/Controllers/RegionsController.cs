﻿using ClienteMvcApi.Models;
using ClienteMvcApi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ClienteMvcApi.Controllers
{
    public class RegionsController : Controller
    {

        IRegionRepositoryGUI _regionRepository;
        IReportRepostoryGUI _reportRepository;

        
        public RegionsController(IRegionRepositoryGUI regionRepository, IReportRepostoryGUI reportRepository)
        {
            _regionRepository = regionRepository;
            _reportRepository = reportRepository;
        }

        public async Task<IActionResult> IndexAsync()
        {
            var regions = await _regionRepository.GetRegionsAsync();
            foreach (var region in regions.Take(5))
            {
                var report = await _reportRepository.GetReportByParamAsync(region.iso);
            }

            ViewBag.Regions = regions;

            return View("Index", new List<RegionDto>());
        }

        
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        
    }
}
